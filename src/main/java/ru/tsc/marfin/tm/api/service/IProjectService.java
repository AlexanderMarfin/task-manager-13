package ru.tsc.marfin.tm.api.service;

import ru.tsc.marfin.tm.enumerated.Status;
import ru.tsc.marfin.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    Project findOneByIndex(Integer index);

    Project findOneById(String id);

    Project updateByIndex(Integer index, String name, String description);

    Project updateById(String id, String name, String description);

    Project remove(Project project);

    Project removeByIndex(Integer index);

    Project removeById(String id);

    void clear();

    List<Project> findAll();

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project changeProjectStatusById(String id, Status status);

}
